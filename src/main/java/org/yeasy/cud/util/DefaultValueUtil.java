package org.yeasy.cud.util;

import org.yeasy.common.util.CommonBeanUtil;
import org.yeasy.cud.annotation.FillNull;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * The type Default value util.
 *
 * @author yangyishe
 * @date 2022年09月22日 10:57
 */
public class DefaultValueUtil {

    /**
     * Fill null.填充默认值
     *
     * @param <T> the type parameter
     * @param obj the obj
     * @author yangyishe
     * @date 2022年09月22日 14:09
     */
    public static<T extends Serializable> void fillNull(T obj){
        Class<?> clazz=obj.getClass();
        Field[] fields=clazz.getDeclaredFields();
        for (Field field : fields) {
            FillNull fillNull=field.getAnnotation(FillNull.class);
            if(fillNull!=null&& CommonBeanUtil.invokeGetter(obj,field.getName())==null){
                Object defaultValue=TypeConverter.convert(fillNull.value(),field.getType());
                CommonBeanUtil.invokeSetter(obj,field.getName(),defaultValue);
            }
        }
    }
}
