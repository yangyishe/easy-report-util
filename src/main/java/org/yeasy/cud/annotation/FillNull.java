package org.yeasy.cud.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Fill null.填充0
 *
 * @author yangyishe
 * @date 2022年09月22日 14:01
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FillNull {
    /**
     * Value string.填充的默认值
     *
     * @return the string
     * @author yangyishe
     * @date 2022年09月22日 14:01
     */
    String value() default "";

}
