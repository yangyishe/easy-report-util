package org.yeasy.cud.constant;


/**
 * The type Cud constant.
 *
 * @author yangyishe
 * @date 2022年09月22日 10:59
 */
public class CudConstant {
    /**
     * The constant DEFAULT_INIT_DATE.
     */
    public static final String DEFAULT_INIT_DATE="1970-01-01";

}
