package org.yeasy.common.constant;

/**
 * The type Common constant.
 *
 * @author yangyishe
 * @date 2022年11月02日 16:35
 */
public class CommonConstant {
    /**
     * The constant VALUE.
     */
    public static final String VALUE="value";
}
