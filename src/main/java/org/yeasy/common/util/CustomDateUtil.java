package org.yeasy.common.util;

import cn.hutool.core.date.DateException;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;

import java.util.Date;

/**
 * The type Custom date util.自定义日期工具
 *
 * @author yangyishe
 * @date 2023年01月09日 16:50
 */
public class CustomDateUtil {
    public final static String NORM_DATE_PATTERN="yyyy-MM-dd";
    public final static String NORM_DATETIME_PATTERN="yyyy-MM-dd HH:mm:ss";
    public final static String TIMEZONE_DATETIME_PATTERN="yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    /**
     * Parse date date.
     *
     * @return the date
     * @author yangyishe
     * @date 2023年01月09日 16:51
     */
    public static Date parseDate(String dateStr){
        // 暂时包括对如下格式进行parse
        // 2022-10-28T16:00:00.000+00:00
        // 2022-10-28
        // 2022-10-28 16:00:00
        if(CharSequenceUtil.isEmpty(dateStr)){
            return null;
        }
        String strDateTrim=dateStr.trim();
        try{
            if(strDateTrim.length()==NORM_DATE_PATTERN.length()){
                return DateUtil.parse(strDateTrim,NORM_DATE_PATTERN);
            }
            if(strDateTrim.length()==NORM_DATETIME_PATTERN.length()){
                return DateUtil.parse(strDateTrim,NORM_DATETIME_PATTERN);
            }
            return DateUtil.parse(strDateTrim,TIMEZONE_DATETIME_PATTERN);
        }catch (DateException e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Convert 2 date date.转化日期
     *
     * @param <T> the type parameter
     * @param t   the t
     * @return the date
     * @author yangyishe
     * @date 2022年08月31日 16:05
     */
    public static <T> Date convert2Date(T t) {
        if (t == null) {
            return null;
        }
        if (t instanceof Date) {
            return (Date) t;
        }
        if (t.getClass() == Long.class) {
            return new Date((Long) t);
        }

        if (t.getClass() == String.class) {
            String strDate = (String) t;
            String strDateFormat = "yyyy-MM-dd";
            if (strDate.length() == strDateFormat.length()) {
                return DateUtil.parse(strDate, strDateFormat);
            }
            String strDatetimeFormat = "yyyy-MM-dd hh:mm:ss";
            if (strDate.length() == strDatetimeFormat.length()) {
                return DateUtil.parse(strDate, strDatetimeFormat);
            }
            throw new DateException("Unknown Date format,can't parse:{}", strDate);
        }
        return null;
    }
}
