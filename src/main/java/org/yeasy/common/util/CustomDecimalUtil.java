package org.yeasy.common.util;

import java.math.BigDecimal;

/**
 * The type Custom decimal util.自定义decimal工具类
 *
 * @author yangyishe
 * @date 2023年02月03日 15:57
 */
public class CustomDecimalUtil {
    /**
     * Convert 2 big decimal big decimal.转换为数字格式
     *
     * @param <T> the type parameter
     * @param t   the t
     * @return the big decimal
     * @author yangyishe
     * @date 2022年08月31日 16:08
     */
    public static <T> BigDecimal convert2BigDecimal(T t) {
        if (t == null) {
            return BigDecimal.ZERO;
        }
        if (t.getClass() == BigDecimal.class) {
            return (BigDecimal) t;
        }
        if (t.getClass() == String.class) {
            return new BigDecimal((String) t);
        }
        return new BigDecimal(t.toString());
    }
}
