package org.yeasy.common.util;

import java.io.Serializable;
import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * The type Custom str util.自定义字符串工具
 *
 * @author yangyishe
 * @date 2023年01月11日 11:01
 */
public class CustomStrUtil {
    /**
     * Sort cn list.
     *
     * @param strList the str list
     * @return the list
     * @author yangyishe
     * @date 2023年01月11日 11:01
     */
    public static List<String> sortCn(List<String> strList){
        return strList.stream().sorted(Collator.getInstance(Locale.CHINA)::compare).collect(Collectors.toList());
    }

    /**
     * Sort cn list.
     *
     * @param <T>   the type parameter
     * @param tList the t list
     * @param field the field
     * @return the list
     * @author yangyishe
     * @date 2023年01月11日 11:06
     */
    public static<T extends Serializable> List<T> sortCn(List<T> tList,String field){
        Comparator<T> comparator= (o1, o2) -> {
            String s1=CommonBeanUtil.invokeGetter(o1,field);
            String s2=CommonBeanUtil.invokeGetter(o2,field);
            return Collator.getInstance(Locale.CHINA).compare(s1,s2);
        };
        return tList.stream().sorted(comparator).collect(Collectors.toList());
    }
}
