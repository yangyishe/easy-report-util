package org.yeasy.eval.constant;

/**
 * The type Eval constant.
 * eval常量
 *
 * @author yangyishe
 * @date 2022年10月20日 10:22
 */
public class EvalConstant {
    /**
     * The constant FORMULA_FACTOR.
     */
    public static final String FORMULA_FACTOR="\\{[\\w.]+}";


    /**
     * The constant SEPARATOR.
     */
    public static final String SEPARATOR=".";

    /**
     * The constant DOC_CODE.
     */
    public static final String DOC_CODE="docCode";

    /**
     * The constant OPENING_DATE.
     */
    public static final String OPENING_DATE="openingDate";
}
