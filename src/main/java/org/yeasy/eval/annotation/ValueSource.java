package org.yeasy.eval.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Value source.值来源的注解
 *
 * @author yangyishe
 * @date 2022年10月25日 09:42
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValueSource {
    /**
     * Value string.调用默认值,默认与field一致
     *
     * @return the string
     * @author yangyishe
     * @date 2022年10月25日 09:42
     */
    String value() default "";

    /**
     * Label string.显示label,可理解为中文
     *
     * @return the string
     * @author yangyishe
     * @date 2022年10月25日 09:42
     */
    String label();

    /**
     * Relation subject relative category enum.关联数据
     *
     * @return the subject relative category enum
     * @author yangyishe
     * @date 2022年10月25日 09:42
     */
    String relation() default "";

    /**
     * Key type key type enum.key的类型,可能为id,code或name
     *
     * @return the key type enum
     * @author yangyishe
     * @date 2022年10月25日 09:42
     */
    String keyType() default "";


    /**
     * Date type string.仅当当前field类型为日期时才需要记录
     *
     * @return the string
     * @author yangyishe
     * @date 2022年10月28日 09:49
     */
    String dateType() default "";
}
