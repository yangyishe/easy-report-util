package org.yeasy.eval.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Doc detail.分录明细
 *
 * @author yangyishe
 * @date 2022年10月25日 09:44
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DetailSource {

    /**
     * Value string.分类称呼,如果为空则与field一致, 但考虑到分录字段较长, 一般建议再缩短
     *
     * @return the string
     * @author yangyishe
     * @date 2022年10月25日 09:44
     */
    String value() default "";

    /**
     * Label string.名称
     *
     * @return the string
     * @author yangyishe
     * @date 2022年10月25日 09:44
     */
    String label();
}
