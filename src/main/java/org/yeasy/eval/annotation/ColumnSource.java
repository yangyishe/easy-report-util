package org.yeasy.eval.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Column source.列来源
 * label的值如果为空, 则默认为DetailSource的label
 *
 * @author yangyishe
 * @date 2022年11月02日 10:30
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnSource {
    /**
     * Value string.如果ValueSource已经标注了value,或要使用原生Name,此处可不填
     *
     * @return the string
     * @author yangyishe
     * @date 2022年11月02日 10:30
     */
    String value() default "";

    /**
     * Label string.如果ValueSource已经标注了label,此处可不填
     *
     * @return the string
     * @author yangyishe
     * @date 2022年11月02日 10:32
     */
    String label() default "";

    /**
     * Exist boolean.是否存在
     *
     * @return the boolean
     * @author yangyishe
     * @date 2022年11月02日 10:30
     */
    boolean exist() default true;

    /**
     * Sort int.排序. 默认排序1000
     *
     * @return the int
     * @author yangyishe
     * @date 2022年11月02日 17:16
     */
    int sort() default 1000;
}
