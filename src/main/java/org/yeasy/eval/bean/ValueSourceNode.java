package org.yeasy.eval.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * The type Value source node.
 *
 * @author yangyishe
 * @date 2022年10月25日 14:54
 */
@Data
public class ValueSourceNode implements Serializable {
    private String value;

    private String fullValue;

    private String label;

    private String fullLabel;

    private String parentValue;

    private Boolean canRefer;

    private String fieldName;

    private String fieldType;

    private Class<?> fieldClazz;

    private String relation;

    private String keyType;

    private String dateType;

}
