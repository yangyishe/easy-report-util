package org.yeasy.redundance.util;

import java.util.List;
import java.util.Map;

/**
 * The interface Fill redundance interface.
 *
 * @param <T> the type parameter
 * @author yangyishe
 * @date 2023年04月11日 16:28
 */
public interface FillRedundanceInterface<T> {

    /**
     * 根据值填充冗余
     * Fill redundance from value fill redundance interface.
     *
     * @param value             the value
     * @param redundanceKeyList the redundance key list
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromValue(Object value,List<String> redundanceKeyList);

    /**
     * 单一信息来源冗余填充
     * Fill redundance fill redundance interface.
     *
     * @param info              the info
     * @param redundanceKeyList the redundance key list
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromSingle(Object info,List<String> redundanceKeyList);

    /**
     * 单一信息来源冗余填充
     * Fill redundance fill redundance interface.
     *
     * @param info                 the info
     * @param targetKey2InfoKeyMap the info key 2 target key map
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromSingle(Object info,Map<String,String> targetKey2InfoKeyMap);

    /**
     * 集合信息来源冗余填充
     * Fill redundance.
     *
     * @param infoList          the info list
     * @param key               the key
     * @param redundanceKeyList the redundance key list
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromList(List<?> infoList, String key, List<String> redundanceKeyList);

    /**
     * Fill redundance from list fill redundance interface.
     *
     * @param infoList          the info list
     * @param keySource         the key source
     * @param redundanceKeyList the redundance key list
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromList(List<?> infoList,Map.Entry<String,String> keySource,List<String> redundanceKeyList);

    /**
     * 集合信息来源冗余填充
     * Fill redundance.
     *
     * @param infoList             the info list
     * @param keySource            the key source
     * @param targetKey2InfoKeyMap the info key 2 target key map
     * @return the fill redundance interface
     * @author yangyishe
     * @date 2023年04月11日 16:28
     */
    FillRedundanceInterface<T> fillRedundanceFromList(List<?> infoList, Map.Entry<String,String> keySource, Map<String,String> targetKey2InfoKeyMap);

}
