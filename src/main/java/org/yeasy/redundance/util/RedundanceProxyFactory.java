package org.yeasy.redundance.util;

import java.util.Collection;

/**
 * The type Redundance proxy factory.
 */
public class RedundanceProxyFactory {

    /**
     * Get instance redundance proxy.
     *
     * @param <T>    the type parameter
     * @param target the target
     * @return the redundance proxy
     */
    public static<T> RedundanceProxy<T> getInstance(T target){
        return new RedundanceProxy<T>(target);
    }

    /**
     * Get batch instance batch redundance proxy.
     *
     * @param <T>    the type parameter
     * @param target the target
     * @return the batch redundance proxy
     */
    public static<T> BatchRedundanceProxy<T> getBatchInstance(Collection<T> target){
        return new BatchRedundanceProxy<>(target);
    }
}
