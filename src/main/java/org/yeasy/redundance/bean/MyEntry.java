package org.yeasy.redundance.bean;

import java.util.Map;

/**
 * The type My entry.
 *
 * @param <K> the type parameter
 * @param <V> the type parameter
 */
public class MyEntry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    /**
     * Instantiates a new My entry.
     *
     * @param key   the key
     * @param value the value
     */
    public MyEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        V old = this.value;
        this.value = value;
        return old;
    }
}
