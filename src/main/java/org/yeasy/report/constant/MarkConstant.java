package org.yeasy.report.constant;

/**
 * The type Default value constant.
 *
 * @author yangyishe
 * @date 2022年05月11日 10:18
 */
public class MarkConstant {

    /**
     * The constant DEFAULT_URL_SEPARATOR_MARK.url中存在的常用分隔符
     */
    public static final String DEFAULT_URL_SEPARATOR_MARK=",";

    /**
     * The constant DEFAULT_IMPORT_SEPARATOR_MARK.导入数据中常用的分隔符
     */
    public static final String DEFAULT_IMPORT_SEPARATOR_MARK="/";

    /**
     * The constant DEFAULT_UNIQUE_CHAIN_SEPARATOR.
     */
    public static final String DEFAULT_UNIQUE_CHAIN_SEPARATOR=" ";



}
