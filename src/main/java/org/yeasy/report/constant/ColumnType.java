package org.yeasy.report.constant;

/**
 * The type Column type.列类型
 * 本组件最常用的常量
 *
 * @author yangyishe
 * @date 2022年09月07日 14:31
 */
public class ColumnType {
    /**
     * The constant TYPE_GROUP.
     */
    public static final int GROUP=1;
    /**
     * The constant TYPE_KEEP.保持字段.
     * 多数情况可以不取.部分group的情况列,如果不取此值,则默认取前面的值;取此值,则默认取后面的值.
     */
    public static final int KEEP=2;
    /**
     * The constant TYPE_CALC.
     */
    public static final int CALC=3;
    /**
     * The constant TYPE_DATE.特殊类型
     */
    public static final int DATE=4;
}
