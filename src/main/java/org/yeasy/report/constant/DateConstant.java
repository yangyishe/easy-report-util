package org.yeasy.report.constant;

public class DateConstant {
    /**
     * The constant DEFAULT_MONTH_PATTERN.默认凭证月标记
     */
    public static final String DEFAULT_MONTH_PATTERN="yyyy-MM";

    /**
     * The constant DEFAULT_MONTH_BEGIN_SUFFIX.
     */
    public static final String DEFAULT_MONTH_BEGIN_SUFFIX="-01";

    /**
     * The constant DEFAULT_DOC_DATE_PART.默认单据编码日期部分的正则表达式
     */
    public static final String DEFAULT_DOC_DATE_PART="yyyyMMdd";

    /**
     * The constant DEFAULT_INIT_DATE.
     */
    public static final String DEFAULT_INIT_DATE="1970-01-01";
}
