package org.yeasy.report.util;

import cn.hutool.core.text.CharSequenceUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Dynamic column util.动态列工具
 * 处理的所有动态对象,都是JSON格式的
 *
 * @author yangyishe
 * @date 2022年08月31日 15:09
 */
public class DynamicColumnUtil {
    /**
     * The constant DYNAMIC_COLUMN_PREFIX.动态列前缀
     */
    private static final String DYNAMIC_COLUMN_PREFIX = "_dyn_";

    /**
     * Calc dynamic field 2 key string.
     *
     * @param field the field
     * @return the string
     * @author yangyishe
     * @date 2022年09月07日 15:15
     */
    public static String calcDynamicField2Key(String field) {
        return isDynamicField(field) ? field.substring(DYNAMIC_COLUMN_PREFIX.length()) : null;
    }

    /**
     * Calc all dynamic fields list.计算所有动态列
     *
     * @param obj the obj
     * @return the list
     * @author yangyishe
     * @date 2022年08月31日 15:15
     */
    public static Set<String> calcJson2DynamicFields(JSONObject obj) {
        return obj.keySet().stream().filter(DynamicColumnUtil::isDynamicField).collect(Collectors.toSet());
    }

    /**
     * Calc dynamic field string.根据key的值(如id的值)计算动态列
     *
     * @param key the key
     * @return the string
     * @author yangyishe
     * @date 2022年08月31日 15:11
     */
    public static String calcKey2DynamicField(String key) {
        return DYNAMIC_COLUMN_PREFIX + key;
    }

    /**
     * Calcc key 2 dynamic field string.
     *
     * @param key        the key
     * @param valueField the value field
     * @return the string
     * @author yangyishe
     * @date 2022年10月17日 10:26
     */
    public static String calcKey2DynamicField(String key, String valueField) {
        return DYNAMIC_COLUMN_PREFIX + valueField + "_" + key;
    }

    /**
     * Is dynamic column boolean.是否是动态列
     *
     * @param field the field
     * @return the boolean
     * @author yangyishe
     * @date 2022年08月31日 15:09
     */
    public static boolean isDynamicField(String field) {
        return CharSequenceUtil.isNotEmpty(field) && CharSequenceUtil.startWith(field, DYNAMIC_COLUMN_PREFIX);
    }

}
