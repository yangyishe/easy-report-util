package org.yeasy.report.bean;

import cn.hutool.core.util.ReflectUtil;
import org.yeasy.report.constant.ColumnType;
import org.yeasy.report.constant.MarkConstant;
import org.yeasy.report.util.ReportBeanUtil;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Report column setting.列设置
 * 最常用的实体类
 *
 * @param <T> the type parameter
 * @author yangyishe
 * @date 2022年08月30日 10:08
 */
@Data
public class ColumnSetting<T> implements Serializable {

    private String field;

    private Integer type;

    private String parentField;

    private String sumField;

    private String accumulationField;

    private String targetField;

    private Integer keepStrategy;

    private T defaultValue;



    /**
     * Instantiates a new Report column setting.
     *
     * @author yangyishe
     * @date 2022年08月30日 10:08
     */
    public ColumnSetting() {
    }

    /**
     * Instantiates a new Report column setting.
     *
     * @param field the key
     * @param type  the type
     * @author yangyishe
     * @date 2022年08月30日 10:08
     */
    public ColumnSetting(String field, Integer type) {
        this.field = field;
        this.type = type;
        if(type==ColumnType.CALC){
            this.defaultValue= (T)BigDecimal.ZERO;
        }
    }

    /**
     * Get instance report column setting.
     *
     * @param <T>   the type parameter
     * @param field the key
     * @param type  the type
     * @return the report column setting
     * @author yangyishe
     * @date 2022年08月30日 10:08
     */
    public static<T> ColumnSetting<T> getInstance(String field, Integer type){
        return new ColumnSetting<T>(field,type);
    }

    /**
     * Get instance report column setting.获取实体类(方便输入)
     *
     * @param <T>   the type parameter
     * @param param the param
     * @return the report column setting
     * @author yangyishe
     * @date 2022年09月01日 10:17
     */
    public static<T> ColumnSetting<T> getInstance(Object[] param){
        String strField=(String)param[0];
        Integer intType=(Integer)param[1];
        ColumnSetting<T> mInstance=new ColumnSetting<>(strField,intType);
        if(param.length>2){
            String strParentField=(String)param[2];
            mInstance.setParentField(strParentField);
        }
        if(param.length>3){
            String strSumField=(String)param[3];
            mInstance.setSumField(strSumField);
        }
        if(param.length>4){
            String strAccumulationField=(String)param[4];
            mInstance.setAccumulationField(strAccumulationField);
        }
        return mInstance;
    }

    /**
     * Get instance list list.获取实体类集合
     *
     * @param paramList the param list
     * @return the list
     * @author yangyishe
     * @date 2022年09月01日 10:17
     */
    public static List<ColumnSetting> getInstanceList(Object[][] paramList){
        return Arrays.stream(paramList).map(ColumnSetting::getInstance).collect(Collectors.toList());
    }


    /**
     * Add parent key report column setting.
     *
     * @param parentField the parent key
     * @return the report column setting
     * @author yangyishe
     * @date 2022年08月30日 10:08
     */
    public ColumnSetting<T> addParentField(String parentField){
        this.parentField=parentField;
        return this;
    }

    /**
     * Add sum key report column setting.
     *
     * @param sumField the sum key
     * @return the report column setting
     * @author yangyishe
     * @date 2022年08月30日 10:08
     */
    public ColumnSetting<T> addSumField(String sumField){
        this.sumField=sumField;
        return this;
    }

    /**
     * Add accumulation field report column setting.
     *
     * @param accumulationField the accumulation field
     * @return the report column setting
     * @author yangyishe
     * @date 2022年09月06日 09:34
     */
    public ColumnSetting<T> addAccumulationField(String accumulationField){
        this.accumulationField=accumulationField;
        return this;
    }

    /**
     * Add keep stratege column setting.增加保持策略
     *
     * @param keepStrategy the keep strategy
     * @return the column setting
     * @author yangyishe
     * @date 2022年09月13日 09:16
     */
    public ColumnSetting<T> addKeepStrategy(Integer keepStrategy){
        this.keepStrategy=keepStrategy;
        return this;
    }

    /**
     * Add default value column setting.
     *
     * @param defaultValue the default value
     * @return the column setting
     * @author yangyishe
     * @date 2022年09月13日 14:56
     */
    public ColumnSetting<T> addDefaultValue(T defaultValue){
        this.defaultValue=defaultValue;
        return this;
    }




}
