package org.yeasy.report.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Dynamic result.
 *
 * @param <T> the type parameter
 * @author yangyishe
 * @date 2022年09月07日 15:26
 */
@Data
public class DynamicResult<T extends Serializable> implements Serializable {

    private List<T> list;

    private List<DynamicColumn> columns;


    public static<T extends Serializable> DynamicResult<T> getEmptyInstance(){
        DynamicResult<T> data=new DynamicResult<>();
        data.setList(new ArrayList<>());
        data.setColumns(new ArrayList<>());
        return data;
    }
}
