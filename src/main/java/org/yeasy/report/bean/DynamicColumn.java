package org.yeasy.report.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * The type Dynamic column.动态列实体类
 *
 * @author yangyishe
 * @date 2022年09月07日 09:56
 */
@Data
public class DynamicColumn implements Serializable {


    private String field;

    private String key;

    private String label;

}
