package org.yeasy.report.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * The enum Column keep strategy enum.
 *
 * @author yangyishe
 * @date 2022年09月09日 15:56
 */
@Getter
public enum ColumnKeepStrategyEnum {
    /**
     * Keep first column keep strategy enum.
     */
    FIRST_ALWAYS(1),
    /**
     * Keep first prior column keep strategy enum.
     */
    FIRST_PRIOR(2),
    /**
     * Keep last column keep strategy enum.
     */
    LAST_ALWAYS(3),
    /**
     * Keep last prior column keep strategy enum.
     */
    LAST_PRIOR(4),
    /**
     * Keep null column keep strategy enum.
     */
    DEFAULT(5),
    ;

    private final Integer type;

    ColumnKeepStrategyEnum(Integer type) {
        this.type = type;
    }

    private static final Map<Integer,ColumnKeepStrategyEnum> TYPE_TO_ENUM_MAP =new HashMap<>();
    static {
        for (ColumnKeepStrategyEnum c : ColumnKeepStrategyEnum.values()) {
            TYPE_TO_ENUM_MAP.put(c.getType(),c);
        }
    }

    /**
     * Type 2 enum column keep strategy enum.
     *
     * @param type the type
     * @return the column keep strategy enum
     * @author yangyishe
     * @date 2022年09月09日 15:59
     */
    public static ColumnKeepStrategyEnum type2Enum(Integer type){
        return TYPE_TO_ENUM_MAP.get(type);
    }
}
