package org.yeasy.test.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TestBean {
    private Long id;
    private String field;
    private Integer type;
    private Date updateTime;

    public TestBean(Long id, String field) {
        this.id = id;
        this.field = field;
    }
}
