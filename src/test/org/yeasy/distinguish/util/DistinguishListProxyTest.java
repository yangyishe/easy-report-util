package org.yeasy.distinguish.util;

import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.yeasy.test.bean.TestBean;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class DistinguishListProxyTest extends TestCase {

    @Test
    public void testDistinguish() {
        TestBean aa = new TestBean(1L, "aa");
        TestBean bb = new TestBean(2L, "bb");
        List<TestBean> oldBeans = Arrays.asList(aa, bb);

        TestBean cc = new TestBean(2L, "cc");
        TestBean dd = new TestBean(null, "dd");
        List<TestBean> newBeans = Arrays.asList(cc, dd);

        DistinguishListProxy<TestBean> distinguish = new DistinguishListProxy<>(TestBean.class, oldBeans, newBeans).distinguish("id");
        List<TestBean> insertList = distinguish.getInsertList();
        List<TestBean> updateList = distinguish.getUpdateList();
        List<TestBean> removeList = distinguish.getRemoveList();

        log.info("insertList:{}",insertList);
        log.info("updateList:{}",updateList);
        log.info("removeList:{}",removeList);


    }
}