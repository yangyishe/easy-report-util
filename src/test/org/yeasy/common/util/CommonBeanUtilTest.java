package org.yeasy.common.util;


import cn.hutool.core.util.ReflectUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.yeasy.redundance.util.RedundanceProxyFactory;
import org.yeasy.report.bean.ColumnSetting;
import org.yeasy.test.bean.TestBean;

import java.util.Arrays;
import java.util.Date;

@Slf4j
public class CommonBeanUtilTest {

    @Test
    public void calcSetterName() {
        TestBean testBean=new TestBean();
        testBean.setField("dsf");
        RedundanceProxyFactory.getInstance(testBean)
                .fillRedundanceFromValue(null, Arrays.asList("field"))
                .fillRedundanceFromValue(1,Arrays.asList("type"))
                .fillRedundanceFromValue(new Date(),Arrays.asList("updateTime"));

        log.info("columnSetting:{}",testBean);
    }
}