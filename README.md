EasyReportUtil

# 介绍
方便报表开发的若干现成方法.

# 软件架构

本项目目的是创建一个由若干注解和静态方法组成的工具类方法.相比其他工具类,本项目有以下特点:
- 更加注重于与项目逻辑紧要关联的方法. 由此的代价就是部分代码并不会放在所有项目均通用, 有可能根据需求修改代码或配置.
- 依赖数个经典工具. 如hutool工具.

# 安装教程

当前项目还未完全成型,代码结构变更较频繁.
如果有使用必要,建议创建一个分支出来.或者仅考虑代码思想即可.

依赖包包括:
- hutool-all: 核心依赖,所有工具方法都需依赖
- fastjson: 核心依赖,处理jsonObject的主要依赖
- lombok: 主要给entity包内的实体类以@Data注解. 如果没有entity, 可以不引入. 
- EvalEx: eval核心依赖. 用以进行字符串eval成数字的转化.

# 方法介绍
## common
核心包, 其他包的部分方法会依赖这个包的工具方法.
提供一些实体类的基础方法, 或基础类型的特殊方法.

### CommonBeanUtil
处理实体类的通用工具类

- newInstance: 根据类创建单个实例. 中间方法.
- calcGetterName: 计算get方法的名称. 中间方法.
- calcSetterName: 计算set方法的名称. 中间方法.
- invokeGetter: 触发get方法. 兼容于JSON对象.
- invokeSetter: 触发set方法. 兼容于JSON对象.
- bean2JsonBatch: 将bean批量转化为json.
- json2BeanBatch: 将json批量转化为bean.
- calc2AllFieldName: 获取实体类所有的fieldName集合. 兼容于JSON对象.

### CustomDateUtil
处理日期相关的工具类
- parseDate: 解析日期的自动方法. 兼容性较好, 支持多种格式. 如果遇到不兼容的情况, 会再考虑兼容格式.
- convert2Date: 将一个可能转化为Date类型的对象, 转化为Date格式. 支持Date/Long/String等多种格式.

### CustomDecimalUtil
处理decimal的工具
- convert2BigDecimal: 将一个可能转化为数字类型的对象, 转为BigDecimal格式.

### JsonBeanUtil
处理json对象的工具类
- convertEntity2JsonByAnnotationAlias: 将实体类根据注解别名转换为json对象(没有注解则以原名替代)
- convertEntityPage2JsonByAnnotationAlias: 将实体类分页数据, 按照注解别名转化为json分页对象.

## cud
对实体类进行增删改时的工具包.
提供一些实体类的较特殊的实用方法.

### BeanListUtil
实体类集合工具类.
主要对实体类内部进行若干操作.

- distinct: 将部分实体类按照指定field进行distinct, 以去重重复对象
- distinguishList: 区分集合. 将集合拆分为待删除/待修改/待新增三个部分. 拆分之后可分别进行delete/update/insert操作.
- fillBatchId: 批量填充id. 依赖于IdUtil工具类.
- fillBatchInfo: 批量填充信息. 如将createUserId批量填充到明细集合中.
- transToStartEnd: 根据pageSize,pageNum和total, 计算开始和结束的rowIndex. (以后等方法丰富后,挪移到PageUtil中)

### DefaultValueUtil
默认值工具类.
- fillNull: 根据`@FillNull`注解和`TypeConverter`, 填充默认值.

### IdUtil
id生成工具类.
特别注意: IdUtil中的SHARD_ID需要自行根据系统分项目设置. 如果都设置成一样的, 反而没有分布式的意义了.

### TypeConverter
字符串类型转换器
- convert: 将字符串转化为对应类型的值
- convertDefault: 根据类型自动计算其默认值.

## eval
进行eval计算的工具包. 
换言之, 如果不用eval操作, 那么工具方法就最好不要放在这里面.

### EntityAnnotationUtil
实体类注解方法. 依赖于本包内的@ColumnSource,@DetailSource,@ValueSource注解.
本工具类内方法尚有许多不成熟, 主要体现在使用场合并不完全明确.

- calc2DeepFieldMap: 根据注解, 将实体类转化为String-Field键值对. 考虑一层深入.
- calc2DeepFieldNode: 根据注解, 将实体类转化为ValueSourceNode集合. 考虑一层深入.
- calc2FieldValue: 计算field的value. 中间方法.
- calc2SurfaceFieldList: 根据注解, 计算实体类的field集合. 不深入.
- calc2SurfaceFieldMap: 根据注解, 计算实体类的string-field键值对. 不深入
- calcAnnotationOrFieldName2Value: 根据注解值或fieldName,计算得出某个field的value
- calcSpecificAnnotation2ValueList: 获取符合要求注解的字段

### EvalUtil
公式计算的方法类.

- calc2FactorUnwrap: 因子解包
- calc2FactorWrap: 因子包装
- calcEntity2NodeFullValue2FieldValueMap: 根据实体类, 获取一个key为value键, 值为object的map
- calcFormula2Factors: 根据公式, 发现所有公式因子. 中间方法
- calcReplace: 计算替换公式为值. 核心方法.
- calcReplaceDefault: 计算替换默认值. 用以验证公式是否合法.
- calcReplaceDefaultBatch: 批量替换默认值. 批量验证公式是否合法.
- evalDecimal: 将字符串计算为数字. 基础方法
- evalString: 将字符串计算为新字符串. 基础方法.

## report
报表工具包.

当前版本的设计思路, 是通过将原始数据与配置数据(ColumnSetting)作为参数, 输出理想的半成品数据或最终数据.
对于一个复杂报表而言, 可能需要分析调用不同的方法. 常见操作包括: 合并, 累加, (分组/分段/分期/树化)求和.

实体类必须实现Serializable接口.
如果存在动态列, 则是通过fastjson实现替代普通的实体类作为返回值的.

### ColumnUtil
列工具. 基础工具类.
- calc2FieldList: 根据ColumnSetting, 计算field集合. 中间方法
- calc2FieldSet: 同上. 中间方法
- calc2GroupKeyFunc: 根据ColumnSetting, 获取groupKey的计算方法.
- clone: 根据ColumnSetting, 克隆对象
- cloneBatch: 根据ColumnSetting, 批量克隆对象
- mergeObj: 根据ColumnSetting的策略, 合并对象

### DynamicColumnUtil
动态列工具. 基础工具类.
动态列根据前缀`_dyn_`进行标记判断

- calcDynamicField2Key: 动态列名解包
- calcJson2DynamicFields: 根据json对象, 获取动态列名集合
- calcKey2DynamicField: 动态列名包装
- isDynamicField: 判断一个列名, 是不是动态列

### DynamicReportUtil
动态报表工具.

- calc2AccumulationGroupMonth: 根据月份分组计算累进值
- calc2DynamicColumn: 计算动态列. 由于动态列在前台并没有智能映射.此方法作为提供智能映射的获取方法.
- calc2DynamicList: 将静态列数据转化为动态列数据, 基本上是所有动态数据转换的起手式方法
- calc2DynamicResult:  计算动态结果. 将动态数据与动态列作为结果返回, 一般是动态转换的收手式方法
- calc2MergeList: 动态对象集合合并
- calc2SumGroupMonth: 根据月分组计算汇总值
- calc2SumGroupSetting: 根据设置分组计算汇总值

### ReportBeanUtil
报表实体类工具类

- mergeObject: 按任意原则合并对象
- resetDecimalValue: 将所有BigDecimal的属性设置为0. 仅适用于普通bean
- resetDecimalValueIfNull: 将所有BigDecimal的属性,如果为null则设置为0. 仅适用于普通bean

### ReportUtil
标准报表工具.
可解决标准报表中的常见问题.

- calc2AccumulationGroupMonth: 根据月份分组计算累进值
- calc2FillEmptyGroupMonth: 根据月份分组填充空白值
- calc2MergeAny: 按照默认策略将对象集合合并为一个对象
- calc2MixBase: 计算混合类型数据的基底
- calc2SumGroupBegin: 计算合并初始数据
- calc2SumGroupMonth: 根据月份分组计算汇总值
- calc2SumGroupSetting: 根据设置分组计算汇总值
- calcAccumulation: 计算累进值
- fillDefaultValue: 填充默认值
- fillDefaultValueBatch: 批量填充默认值

### TreeReportUtil
树化报表工具.
专门解决存在树化求和问题的工具类.

- calcKey2Offspring: 计算所有祖先后代关联值
- calcLeaf2FullLevel: 将叶节点数据扩展至所有level

end
